package com.da.accons.interfaces

import java.util.*


/**
 * Created by Dimas Alamsyah on 2019-10-14
 * dimasalamsyah0712@gmail.com
 */

interface SubmitInterface {
    fun submit(obj: Any)
}
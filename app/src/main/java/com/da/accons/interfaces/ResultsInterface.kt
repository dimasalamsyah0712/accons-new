package com.da.accons.interfaces


/**
 * Created by Dimas Alamsyah on 2019-10-15
 * dimasalamsyah0712@gmail.com
 */

interface ResultsInterface {
    fun onSuccess(msg : String)
    fun onError(msg : String)
    fun onCompleted()
}
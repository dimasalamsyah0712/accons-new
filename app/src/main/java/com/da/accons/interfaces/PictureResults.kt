package com.da.accons.interfaces

import android.graphics.Bitmap

/**
 * Created by Dimas Alamsyah on 10/19/2019.
 */
interface PictureResults {
    fun imageResults(bitmap : Bitmap)
}
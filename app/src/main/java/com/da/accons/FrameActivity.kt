package com.da.accons

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.da.accons.databinding.ActivityFrameBinding
import com.da.accons.fragment.HistoryFragment
import com.da.accons.fragment.TransactionFragment
import android.app.Activity
import android.content.Intent
import android.view.View
import com.da.accons.fragment.CategoryFragment
import com.da.accons.fragment.SettingFragment


class FrameActivity : BaseActivity() {

    var mFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_frame)
        var binding : ActivityFrameBinding = DataBindingUtil.setContentView(this, R.layout.activity_frame)

        val menu = intent.getStringExtra("menu")

        //setFragement( TransactionFragment(), menu )

        when(menu){
            "in" -> {
                setFragement( TransactionFragment(), menu )
            }
            "out" -> {
                setFragement( TransactionFragment(), menu )
            }
            "history" -> {
                setFragement( HistoryFragment(), menu )
            }
            "category" -> {
                setFragement( CategoryFragment(), menu )
            }
            "setting" -> {
                setFragement( SettingFragment(), menu )
            }
        }

        binding.ivBack.setOnClickListener { finish() }
        binding.tvBack.setOnClickListener { finish() }

    }

    fun setFragement(fragment: Fragment, type: String){

        val bundle = Bundle()
        bundle.putString("type", type)
        fragment.arguments = bundle

        mFragment = fragment

        var fragmentTransaction : FragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameLayout, fragment)
        fragmentTransaction.commit()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK){

            if( requestCode == REQUEST_IMAGE_CAPTURE ){
                mFragment?.onActivityResult(requestCode, resultCode, data)
            }

        }

    }

}

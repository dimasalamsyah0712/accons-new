package com.da.accons

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.da.accons.Libs.Dialog.DA_Dialog
import com.da.accons.databinding.ActivityLoginBinding
import com.da.accons.interfaces.ResultsInterface
import com.da.accons.presenters.SharedPresenter
import com.da.accons.presenters.UserPresenter

class LoginActivity : AppCompatActivity(), ResultsInterface {

    override fun onSuccess(msg: String) {

        val sp = SharedPresenter(this@LoginActivity)
        sp.set("LOGIN", true)
        sp.set("EMAIL", msg)

        dialog.dismiss()

        var intent = Intent(this@LoginActivity, MainNewActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

        startActivity( intent )
    }

    override fun onError(msg: String) {
        dialog.setContent(msg, "Close", R.drawable.ic_wrong)
    }

    override fun onCompleted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    lateinit var presenter: UserPresenter
    lateinit var dialog: DA_Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_login)
        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        presenter = UserPresenter(this)
        dialog = DA_Dialog(this)


        binding.btnLogin.setOnClickListener{

            presenter.get(binding.etUsername.text.toString(), binding.etPassword.text.toString())

            dialog.DialogStandart(this, View.OnClickListener {
                dialog.dismiss()
            }).showLoading()

        }


    }
}

package com.da.accons.presenters

import android.util.Log
import com.da.accons.interfaces.ResultsInterface
import com.da.accons.interfaces.SubmitInterface
import com.da.accons.models.Category
import com.da.accons.models.Results
import com.da.accons.models.Transaction
import com.da.accons.res.ApiClient
import com.da.accons.res.ApiInterface
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Dimas Alamsyah on 10/26/2019.
 */
class CategoryPresenter(rs : ResultsInterface) : SubmitInterface {

    private var sr : ResultsInterface
    private var apiInterface: ApiInterface
    private lateinit var data: ArrayList<Category>

    init {
        sr = rs
        apiInterface = ApiClient().getClient()!!.create(ApiInterface::class.java)
    }

    fun get(email: String){

        apiInterface.getCategory(email)
            .subscribeOn( Schedulers.newThread() )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> handleRespons(result) },
                { error -> handleError(error) }
            )

    }

    fun post(category: Category): Int{
        if(category.Email == ""){
            handleErrorInput("Email required")
            return 1
        }else if(category.Category == ""){
            handleErrorInput("Category required")
            return 2
        }

        apiInterface.postCategory(category.Email, category.Category)
            .subscribeOn( Schedulers.newThread() )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> handleRespons(result) },
                { error -> handleError(error) }
            )

        return -1
    }

    fun getData(): ArrayList<Category>{
        return data
    }

    override fun submit(obj: Any) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun handleErrorInput(msg: String){
        sr.onError(msg)
    }

    fun handleError(error: Throwable){
        Log.d("DEBUG->", error.localizedMessage)

        sr.onError(error.localizedMessage)
    }

    fun handleRespons(result: Any){

        if(result is Results){

            val gson = Gson()
            val itemType = object : TypeToken<ArrayList<Category>>(){}.type
            data = gson.fromJson(gson.toJson(result.data), itemType)

            sr.onSuccess(result.message)

        }

    }

}
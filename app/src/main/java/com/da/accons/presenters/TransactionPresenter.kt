package com.da.accons.presenters

import android.util.Log
import com.da.accons.interfaces.SubmitInterface
import com.da.accons.interfaces.ResultsInterface
import com.da.accons.models.Results
import com.da.accons.models.Summary
import com.da.accons.models.Transaction
import com.da.accons.res.ApiClient
import com.da.accons.res.ApiInterface
import com.google.gson.Gson
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers;
import kotlin.collections.ArrayList
import com.google.gson.reflect.TypeToken




/**
 * Created by Dimas Alamsyah on 2019-10-14
 * dimasalamsyah0712@gmail.com
 */

class TransactionPresenter(rs : ResultsInterface) : SubmitInterface {

    private var sr : ResultsInterface
    private var apiInterface: ApiInterface
    private lateinit var data: ArrayList<Transaction>
    private lateinit var summary: Summary

    init {
        sr = rs
        apiInterface = ApiClient().getClient()!!.create(ApiInterface::class.java)
    }

    override fun submit(obj: Any) {

        var model= obj as Transaction

        apiInterface.
            postTransaction( model.Email, model.Nominal, model.Category, model.Description, model.Picture, model.Type)
            .subscribeOn( Schedulers.newThread() )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> handleSubmitRespons(result) },
                { error -> handleError(error) }
            )

    }

    fun get(email: String){

        apiInterface.getTransaction(email)
            .subscribeOn( Schedulers.newThread() )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> handleRespons(result) },
                { error -> handleError(error) }
            )

    }

    fun getData(): ArrayList<Transaction>{
        return data
    }

    fun getSummary(): Summary{
        return summary
    }

    fun getSum(type: String, email: String){

        apiInterface.getSum(type, email)
            .subscribeOn( Schedulers.newThread() )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> handleResponsSum(result) },
                { error -> handleError(error) }
            )

    }

    fun handleResponsSum(result: Any){

        if(result is Summary ){

            summary = result

            sr.onSuccess(result.message)
        }

    }

    fun handleError(error: Throwable){
        Log.d("DEBUG->", error.localizedMessage)

        sr.onError(error.localizedMessage)
    }

    fun handleSubmitRespons(rs: Any){
        var result = rs as Results
        sr.onSuccess(result.message)
    }

    fun handleRespons(result: Any){

        if(result is Results ){

            val gson = Gson()
            val itemType = object : TypeToken<ArrayList<Transaction>>(){}.type
            data = gson.fromJson(gson.toJson(result.data), itemType)

            sr.onSuccess(result.message)

        }

    }

}
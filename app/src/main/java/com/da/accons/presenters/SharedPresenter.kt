package com.da.accons.presenters

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Dimas Alamsyah on 10/27/2019.
 */
class SharedPresenter(val context: Context) {

    private val PREF_LOGIN = "login"
    val sp: SharedPreferences = context.getSharedPreferences(PREF_LOGIN, Context.MODE_PRIVATE)

    fun set(KEY_NAME: String, status: Boolean) {
        val editor: SharedPreferences.Editor = sp.edit()
        editor.putBoolean(KEY_NAME, status!!)
        editor.commit()
    }

    fun set(KEY_NAME: String, status: String) {
        val editor: SharedPreferences.Editor = sp.edit()
        editor.putString(KEY_NAME, status!!)
        editor.commit()
    }

    fun get(KEY_NAME: String, defaultValue: String): String {
        return sp.getString(KEY_NAME, defaultValue)
    }

    fun get(KEY_NAME: String, defaultValue: Boolean): Boolean {
        return sp.getBoolean(KEY_NAME, defaultValue)
    }

    fun clear() {
        val editor: SharedPreferences.Editor = sp.edit()
        editor.clear()
        editor.commit()
    }

    fun removeValue(KEY_NAME: String) {
        val editor: SharedPreferences.Editor = sp.edit()
        editor.remove(KEY_NAME)
        editor.commit()
    }

}
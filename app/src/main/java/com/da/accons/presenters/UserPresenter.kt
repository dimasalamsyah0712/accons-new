package com.da.accons.presenters

import android.content.Context
import android.util.Log
import com.da.accons.interfaces.ResultsInterface
import com.da.accons.interfaces.SubmitInterface
import com.da.accons.models.Results
import com.da.accons.models.Transaction
import com.da.accons.models.Users
import com.da.accons.res.ApiClient
import com.da.accons.res.ApiInterface
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Dimas Alamsyah on 10/27/2019.
 */
class UserPresenter(rs : ResultsInterface): SubmitInterface {

    private var sr : ResultsInterface
    private var apiInterface: ApiInterface
    private lateinit var data: ArrayList<Users>
    private lateinit var user: Users

    init {
        sr = rs
        apiInterface = ApiClient().getClient()!!.create(ApiInterface::class.java)
    }

    override fun submit(obj: Any) {

    }

    fun setUser(user: Users){
        this.user = user
    }

    fun getUser(): Users{
        return user
    }

    fun get(email: String, password: String){

        val user = Users(email, "", password, "")

        setUser(user)

        apiInterface.getUser(email)
            .subscribeOn( Schedulers.newThread() )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> handleRespons(result) },
                { error -> handleError(error) }
            )

    }

    fun handleError(error: Throwable){
        Log.d("DEBUG->", error.localizedMessage)

        sr.onError(error.localizedMessage)
    }

    fun handleRespons(result: Any){

        if(result is Results){

            val gson = Gson()
            val itemType = object : TypeToken<ArrayList<Users>>(){}.type
            data = gson.fromJson(gson.toJson(result.data), itemType)

            if(data.size > 0){
                if(user.Email == data.get(0).Email && user.Password == data.get(0).Password ){
                    sr.onSuccess(user.Email)
                }else{
                    sr.onError("username or password wrong!")
                }
            }else{
                sr.onError("username or password wrong!")
            }



        }

    }

}
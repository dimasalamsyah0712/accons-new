package com.da.accons.models


/**
 * Created by Dimas Alamsyah on 2019-10-14
 * dimasalamsyah0712@gmail.com
 */

data class Transaction(var Email : String, var Nominal : Double, var Balance : Double, var Category : String,
                       var Description : String, var Picture : String, var Type : String, var CreatedAt : String)
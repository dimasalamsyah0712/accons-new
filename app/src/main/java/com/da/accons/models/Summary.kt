package com.da.accons.models

/**
 * Created by Dimas Alamsyah on 10/20/2019.
 */
data class Summary(var status: String, var message: String, var sumIn: Double, var sumOut: Double) {
}
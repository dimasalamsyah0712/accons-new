package com.da.accons.models

/**
 * Created by Dimas Alamsyah on 10/26/2019.
 */
data class Category(var Email : String, var Category : String, var CreatedAt : String)
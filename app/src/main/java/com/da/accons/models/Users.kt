package com.da.accons.models

/**
 * Created by Dimas Alamsyah on 10/27/2019.
 */
data class Users(var Email: String, var Name: String, var Password: String, var ProfileImage: String) {
}
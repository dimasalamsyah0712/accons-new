package com.da.accons.models

import java.util.*

/**
 * Created by Dimas Alamsyah on 10/15/2019.
 */
data class Results(var message : String, var status : String, var data : Any) {
}
package com.da.accons

import android.os.Bundle
import android.content.Intent
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import com.da.accons.databinding.ActivityMainNewBinding
import com.da.accons.interfaces.ResultsInterface
import com.da.accons.presenters.SharedPresenter
import com.da.accons.presenters.TransactionPresenter
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class MainNewActivity : BaseActivity(), ResultsInterface {

    override fun onCompleted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(msg: String) {

        var summary = tp.getSummary()

        tvSumIn?.text = "+ Rp "+ format(summary.sumIn.toDouble())
        tvSumOut?.text = "- Rp "+ format(summary.sumOut.toDouble())

    }

    override fun onError(msg: String) {
        Log.d("DEBUG->", msg)
    }

    fun format(s: Double): String? {
        val df = DecimalFormat("#,###.##")
        return df.format(s)
    }

    var tvSumIn: TextView? = null
    var tvSumOut: TextView? = null
    lateinit var tp: TransactionPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main_new)
        val binding: ActivityMainNewBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_new)

        val isOnline = isOnline(this)
        Log.d("DEBUG->", isOnline.toString())

        var sp = SharedPresenter(this@MainNewActivity)
        var login = sp.get("LOGIN", false)

        if(!login){
            val intent = Intent(this@MainNewActivity, LoginActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }

        //var simple = SimpleDateFormat("MMMMM", Locale.US)
        var calender = Calendar.getInstance()

        binding.tvCurrentMonth.text = calender.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())

        tvSumIn = binding.tvSumIn
        tvSumOut = binding.tvSumOut

        tvSumIn?.text = "+ Rp 0"
        tvSumOut?.text = "- Rp 0"

        var email = sp?.get("EMAIL", "")

        tp = TransactionPresenter(this)
        tp.getSum("all", email.toString())

        binding.btnIn.setOnClickListener {
            intent = Intent(this@MainNewActivity, FrameActivity::class.java)
            intent.putExtra("menu", "in")
            startActivity(intent)
        }

        binding.btnOut.setOnClickListener {
            intent = Intent(this@MainNewActivity, FrameActivity::class.java)
            intent.putExtra("menu", "out")
            startActivity(intent)
        }

        binding.ivHistory.setOnClickListener {
            intent = Intent(this@MainNewActivity, FrameActivity::class.java)
            intent.putExtra("menu", "history")
            startActivity(intent)
        }

        binding.ivCategory.setOnClickListener {
            intent = Intent(this@MainNewActivity, FrameActivity::class.java)
            intent.putExtra("menu", "category")
            startActivity(intent)
        }

        binding.ivSetting.setOnClickListener {
            intent = Intent(this@MainNewActivity, FrameActivity::class.java)
            intent.putExtra("menu", "setting")
            startActivity(intent)
        }

    }

}

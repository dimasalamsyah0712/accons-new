package com.da.accons.fragment


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.da.accons.LoginActivity

import com.da.accons.R
import com.da.accons.databinding.FragmentSettingBinding
import com.da.accons.presenters.SharedPresenter

/**
 * A simple [Fragment] subclass.
 */
class SettingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding : FragmentSettingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false)

         binding.tvLogout.setOnClickListener {

             var sp = context?.let { it1 -> SharedPresenter(it1) }
             sp?.clear()
             activity?.intent = Intent(context, LoginActivity::class.java)
             activity?.intent?.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity( activity?.intent )

         }

        return binding.root
    }


}

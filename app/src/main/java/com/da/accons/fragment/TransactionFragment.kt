package com.da.accons.fragment


import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import com.da.accons.FrameActivity
import com.da.accons.Libs.DA_Picture
import com.da.accons.Libs.Dialog.DA_Dialog

import com.da.accons.R
import com.da.accons.databinding.FragmentTransactionBinding
import com.da.accons.interfaces.ResultsInterface
import com.da.accons.models.Transaction
import com.da.accons.presenters.SharedPresenter
import com.da.accons.presenters.TransactionPresenter
import java.lang.Exception
import java.text.DecimalFormat

/**
 * A simple [Fragment] subclass.
 */
class TransactionFragment : Fragment(), ResultsInterface{

    var dialog: DA_Dialog? = null
    var custom: DA_Dialog.CustomDialog? = null
    var daPicture: DA_Picture? = null
    lateinit var imageView: ImageView
    lateinit var etCategory: EditText
    var mNominal: String = "0"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding : FragmentTransactionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_transaction, container, false)
        imageView = binding.ivPicture
        etCategory = binding.etCategory

        daPicture = DA_Picture(context)

        dialog = DA_Dialog(context)
        custom = dialog?.CustomDialog(activity, View.OnClickListener {
            activity?.finish()
        })
        custom?.getProgress()

        binding.ivPicture.setOnClickListener {
            daPicture?.takePicture()
        }



        binding.etNominal.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(!s.toString().equals("")){

                    if(mNominal.toDouble() >= 0){

                        mNominal = s.toString().replace(",", "")
                        Log.i("DEBUG", mNominal )

                        binding.etNominal.removeTextChangedListener(this)

                        binding.etNominal.text = format(mNominal.toDouble())?.toEditable()
                        binding.etNominal.setSelection(format(mNominal.toDouble())?.length!!)

                        binding.etNominal.addTextChangedListener(this)

                    }

                }
            }

        })

        binding.btnSubmit.setOnClickListener {

            try {

                val sp = context?.let { SharedPresenter(it) }
                var email = sp?.get("EMAIL", "")

                var type = arguments?.getString("type")

                var model = Transaction(email.toString(),
                    mNominal.toDouble() ?: fail("Name required") ,
                    0.0,
                    binding.etCategory?.text.toString(),
                    binding.etDescription?.text.toString(),
                    "picture.jpg",
                    type.toString(),
                    ""
                )

                dialog?.show()
                var tp = TransactionPresenter(this)
                tp.submit(model)

            }
            catch (e: Exception) {

                var dialog2 = DA_Dialog(context)
                var custom2 = dialog2?.CustomDialog(activity, View.OnClickListener {
                    dialog2.dismiss()
                })
                custom2.getProgress()
                dialog2.show()
                custom2?.setDialog(e.localizedMessage, R.drawable.ic_wrong)
                custom2?.showMsg()
            }


        }

        binding.etCategory.setOnClickListener {
            activity?.intent = Intent(activity, FrameActivity::class.java)
            activity?.intent?.putExtra("menu", "category")
            startActivityForResult(activity?.intent, 111)
        }

        return binding.root
    }

    fun format(s: Double): String? {
        val df = DecimalFormat("#,###.##")
        return df.format(s)
    }

    fun fail(msg: String): Nothing {
        custom?.setDialog(msg, R.drawable.ic_wrong)
        custom?.showMsg()

        throw IllegalArgumentException(msg)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 11){
            var bitmap : Bitmap = BitmapFactory.decodeFile( daPicture?.getPicturePath() )
            imageView.setImageBitmap(bitmap)
        }
        //data.getStringExtra("category")
        if (requestCode == 111) {
            etCategory.text = data?.getStringExtra("category")?.toEditable()
        }

    }

    fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)

    override fun onSuccess(msg: String) {
        custom?.setDialog(msg, R.drawable.ic_ok_48)
        custom?.showMsg()
    }

    override fun onError(msg: String) {
        custom?.setDialog(msg, R.drawable.ic_wrong)
        custom?.showMsg()
    }

    override fun onCompleted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

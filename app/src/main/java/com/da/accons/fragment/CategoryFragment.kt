package com.da.accons.fragment


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.da.accons.Libs.Dialog.DA_Dialog
import com.da.accons.Libs.StringUtil

import com.da.accons.R
import com.da.accons.adapter.CategoryAdapter
import com.da.accons.adapter.TransactionAdapter
import com.da.accons.databinding.FragmentCategoryBinding
import com.da.accons.interfaces.ResultsInterface
import com.da.accons.models.Category
import com.da.accons.models.Transaction
import com.da.accons.presenters.CategoryPresenter
import com.da.accons.presenters.SharedPresenter
import com.da.accons.presenters.TransactionPresenter

/**
 * A simple [Fragment] subclass.
 */
class CategoryFragment : Fragment(), ResultsInterface {

    var recyclerView: RecyclerView? = null
    lateinit var presenter: CategoryPresenter
    var categories: ArrayList<Category> = ArrayList<Category>()
    var adapter: CategoryAdapter? = null
    var dialog: DA_Dialog? = null
    var custom: DA_Dialog.CustomDialog? = null
    lateinit var binding: FragmentCategoryBinding

    override fun onSuccess(msg: String) {
        //binding.etCategory.text = msg.toEdit()
        categories = presenter.getData()
        adapter?.update(categories)
    }

    override fun onError(msg: String) {
        custom?.setDialogWithBtn(msg, "Close", R.drawable.ic_wrong)
        custom?.showMsg()
    }

    override fun onCompleted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun String.toEdit(): Editable =  Editable.Factory.getInstance().newEditable(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_category, container, false)
        recyclerView = binding.recyclerView

        dialog = DA_Dialog(context)
        custom = dialog?.CustomDialog(activity, View.OnClickListener {

        })
        //custom?.getProgress()
        //dialog?.show()

        presenter = CategoryPresenter(this)

        adapter = activity?.let { CategoryAdapter(categories, it) }
        recyclerView?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recyclerView?.adapter = adapter

        val sp = context?.let { SharedPresenter(it) }
        var email = sp?.get("EMAIL", "")

        presenter.get(email.toString())

        binding.ivSave.setOnClickListener {
            presenter.post( Category("dimasalamsyah0712@gmail.com", binding.etCategory.text.toString(), "") )
        }

        return binding.root

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

}

package com.da.accons.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.da.accons.Libs.Dialog.DA_Dialog

import com.da.accons.R
import com.da.accons.adapter.TransactionAdapter
import com.da.accons.databinding.FragmentHistoryBinding
import com.da.accons.databinding.LayoutListHistoryBinding
import com.da.accons.interfaces.ResultsInterface
import com.da.accons.models.Transaction
import com.da.accons.presenters.TransactionPresenter

/**
 * A simple [Fragment] subclass.
 */
class HistoryFragment : Fragment(), ResultsInterface {

    override fun onSuccess(msg: String) {

        dialog?.dismiss()

        transactions = transaction.getData()
        adapter?.update(transactions)

        //recyclerView?.layoutManager?.scrollToPosition(transactions.size - 1)
        //(recyclerView?.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(10, 10)
    }

    override fun onError(msg: String) {
        custom?.setDialogWithBtn(msg, "Close!", R.drawable.ic_wrong)
        custom?.showMsg()
    }

    override fun onCompleted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var recyclerView: RecyclerView? = null
    lateinit var transaction: TransactionPresenter
    var transactions: ArrayList<Transaction> = ArrayList<Transaction>()
    var adapter: TransactionAdapter? = null
    var dialog: DA_Dialog? = null
    var custom: DA_Dialog.CustomDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.layout_list_history, container, false)

        val binding : FragmentHistoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false)
        recyclerView = binding.recyclerView

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        transaction = TransactionPresenter(this)

        dialog = DA_Dialog(context)
        custom = dialog?.CustomDialog(activity, View.OnClickListener {
            dialog?.dismiss()

        })
        custom?.getProgress()
        dialog?.show()

        transaction.get("dimasalamsyah0712@gmail.com")

        adapter = TransactionAdapter(transactions)
        recyclerView?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recyclerView?.adapter = adapter

    }

}

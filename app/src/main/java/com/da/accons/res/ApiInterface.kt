package com.da.accons.res

import com.da.accons.models.Results
import com.da.accons.models.Summary
import retrofit2.http.*
import rx.Observable

/**
 * Created by Dimas Alamsyah on 10/15/2019.
 */
interface ApiInterface {

    @FormUrlEncoded
    @POST("transaction")
    fun postTransaction (@Field("email") email: String,
                         @Field("nominal") nominal: Double,
                         @Field("category") category: String,
                         @Field("description") description: String,
                         @Field("picture") picture: String,
                         @Field("type") type: String) : Observable< Results >

    @GET("/transaction/{email}")
    fun getTransaction (@Path("email") email: String): Observable< Results >

    @GET("/{type}/{email}")
    fun getSum (@Path("type") type: String,
                @Path("email") email: String): Observable< Summary >

    @GET("/category/{email}")
    fun getCategory (@Path("email") email: String): Observable< Results >

    @FormUrlEncoded
    @POST("category")
    fun postCategory(@Field("email") email: String,
                     @Field("category") category: String) : Observable< Results >

    @GET("/user/{email}")
    fun getUser (@Path("email") email: String): Observable< Results >
}
package com.da.accons.res

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Dimas Alamsyah on 10/15/2019.
 */
class ApiClient {
    public final val BASE_URL = "https://dalamsyah.rizqisyariah.com/"
    private var retrofit : Retrofit? = null

    public fun getClient() : Retrofit? {
        if (retrofit == null){
            var gson = GsonBuilder().setLenient().create()

            retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        }

        return retrofit
    }

}
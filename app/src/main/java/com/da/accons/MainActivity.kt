package com.da.accons

import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.viewpager.widget.ViewPager
import com.da.accons.adapter.MyPagerAdapter
import com.da.accons.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.viewPager.adapter = MyPagerAdapter(supportFragmentManager)

        binding.navigasi.setOnNavigationItemSelectedListener { item ->
            when (item.itemId){
                R.id.action_home -> {
                    binding.viewPager.setCurrentItem(0, true);
                }
                R.id.action_out -> {
                    binding.viewPager.setCurrentItem(1, true);
                }
                R.id.action_in -> {
                    binding.viewPager.setCurrentItem(1, true);
                }
                R.id.action_setting -> {
                    binding.viewPager.setCurrentItem(2, true);

                }
                else -> binding.viewPager.setCurrentItem(2, true);
            }

            return@setOnNavigationItemSelectedListener true
        }

        binding.viewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {

                /*if (position == 0)
                    binding.navigasi.setSelectedItemId(R.id.action_home);
                if (position == 1)
                    binding.navigasi.setSelectedItemId(R.id.action_in);
                if (position == 1)
                    binding.navigasi.setSelectedItemId(R.id.action_in);
                if (position == 2)
                    binding.navigasi.setSelectedItemId(R.id.action_setting);*/

            }

        })

    }
}

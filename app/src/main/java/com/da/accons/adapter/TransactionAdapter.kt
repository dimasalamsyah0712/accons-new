package com.da.accons.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.da.accons.R
import com.da.accons.models.Transaction
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Dimas Alamsyah on 10/19/2019.
 */
class TransactionAdapter(private val list: ArrayList<Transaction>): RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    var mHistoryDate: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TransactionViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val model = list.get(position)
        holder.bind(model)

        if(mHistoryDate.equals(holder.historyDate?.text.toString())){

            holder.historyDate?.visibility = View.GONE

            Log.d("DEBUG->", "sama "+ holder.historyDate?.text.toString() )

        }else{
            Log.d("DEBUG->", "beda "+ holder.historyDate?.text.toString()+" dengan "+mHistoryDate )

            holder.historyDate?.visibility = View.VISIBLE
            mHistoryDate = holder.historyDate?.text.toString()

        }

    }

    fun update(data: ArrayList<Transaction>){
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    class TransactionViewHolder(inflater: LayoutInflater, parent: ViewGroup):
        RecyclerView.ViewHolder(inflater.inflate(R.layout.layout_list_history, parent, false)){

        private var tvNominalIn: TextView? = null
        private var tvNominalOut: TextView? = null

        private var tvCategoryIn: TextView? = null
        private var tvCategoryOut: TextView? = null

        private var tvDescIn: TextView? = null
        private var tvDescOut: TextView? = null

        private var tvTimeOut: TextView? = null
        private var tvTimeIn: TextView? = null

        private var tvBalanceIn: TextView? = null
        private var tvBalanceOut: TextView? = null

        private var ivAttechIn: ImageView? = null
        private var ivAttechOut: ImageView? = null

        private var layoutIn: ConstraintLayout? = null;
        private var layoutOut: ConstraintLayout? = null;

        var historyDate: TextView? = null

        init {
            tvNominalIn = itemView.findViewById(R.id.tvNominalIn)
            tvNominalOut = itemView.findViewById(R.id.tvNominalOut)
            tvCategoryIn = itemView.findViewById(R.id.tvCategoryIn)
            tvCategoryOut = itemView.findViewById(R.id.tvCategoryOut)
            tvDescIn = itemView.findViewById(R.id.tvDescIn)
            tvDescOut = itemView.findViewById(R.id.tvDescOut)
            ivAttechIn = itemView.findViewById(R.id.ivAttechIn)
            ivAttechOut = itemView.findViewById(R.id.ivAttechOut)
            layoutIn = itemView.findViewById(R.id.layout_in)
            layoutOut = itemView.findViewById(R.id.layout_out)
            historyDate = itemView.findViewById(R.id.tvHistoryDate)
            tvTimeOut = itemView.findViewById(R.id.tvTimeOut)
            tvTimeIn = itemView.findViewById(R.id.tvTimeIn)
            tvBalanceIn = itemView.findViewById(R.id.tvBalanceIn)
            tvBalanceOut = itemView.findViewById(R.id.tvBalanceOut)

        }

        fun bind(transaction: Transaction){
            tvNominalIn?.text = format( transaction.Nominal.toString() )
            tvNominalOut?.text = format( transaction.Nominal.toString() )
            tvCategoryIn?.text = transaction.Category.toString()
            tvCategoryOut?.text = transaction.Category.toString()
            tvDescIn?.text = transaction.Description.toString()
            tvDescOut?.text = transaction.Description.toString()
            tvBalanceIn?.text = format( transaction.Balance.toString() )
            tvBalanceOut?.text = format( transaction.Balance.toString() )

            var simple = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).parse(transaction.CreatedAt)
            var format = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(simple)
            var formatIn = SimpleDateFormat("HH:mm:ss", Locale.US).format(simple)
            var formatOut = SimpleDateFormat("HH:mm:ss", Locale.US).format(simple)


            historyDate?.text = format
            tvTimeIn?.text = formatIn
            tvTimeOut?.text = formatOut

            if(transaction.Type.equals("in")){
                layoutOut?.visibility = View.GONE
                layoutIn?.visibility = View.VISIBLE

            }else{
                layoutIn?.visibility = View.GONE
                layoutOut?.visibility = View.VISIBLE
            }

        }

        fun format(s: String): String? {
            val df = DecimalFormat("#,###.##")
            return df.format(s.toDouble())
        }

    }

}



package com.da.accons.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.da.accons.FrameActivity
import com.da.accons.R
import com.da.accons.models.Category
import com.da.accons.models.Transaction

/**
 * Created by Dimas Alamsyah on 10/26/2019.
 */
class CategoryAdapter(private val list: ArrayList<Category>, private val activity: Activity): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CategoryViewHolder(inflater, parent, activity)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val model = list.get(position)
        holder.bind(model)
    }

    fun update(data: ArrayList<Category>){
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    class CategoryViewHolder(inflater: LayoutInflater, parent: ViewGroup, activity: Activity):
        RecyclerView.ViewHolder(inflater.inflate(R.layout.layout_list_category, parent, false)){

        var tvCategory: TextView? = null
        init {
            tvCategory = itemView.findViewById(R.id.tvCategory)

            itemView.setOnClickListener {
                activity.intent = activity.intent.putExtra("category", tvCategory?.text.toString())
                activity.setResult(Activity.RESULT_OK, activity.intent)
                activity.finish()
            }

        }

        fun bind(category: Category){
            tvCategory?.text = category.Category
        }


    }

}
package com.da.accons.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.da.accons.fragment.HomeFragment
import com.da.accons.fragment.SettingFragment
import com.da.accons.fragment.TransactionFragment


/**
 * Created by Dimas Alamsyah on 2019-10-12
 * dimasalamsyah0712@gmail.com
 */

class MyPagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {

    // sebuah list yang menampung objek Fragment
    private val pages = listOf(
        HomeFragment(),
        TransactionFragment(),
        SettingFragment()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    // judul untuk tabs
    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Home"
            1 -> "Out"
            1 -> "In"
            2-> "Setting"
            else -> "Setting"
        }
    }

}
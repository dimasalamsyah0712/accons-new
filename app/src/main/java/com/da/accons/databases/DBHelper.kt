package com.da.accons.databases

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

/**
 * Created by Dimas Alamsyah on 11/4/2019.
 */
class DBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_TRANSACTION)
        db.execSQL(SQL_CREATE_CATEGORY)
    }
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_TRANSACTION)
        db.execSQL(SQL_DELETE_CATEGORY)
        onCreate(db)
    }
    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }
    companion object {
        // If you change the database schema, you must increment the database version.
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "accons.db"

        private const val SQL_CREATE_TRANSACTION =
            "CREATE TABLE ${AppTable.Transaction.TABLE_NAME} (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${AppTable.Transaction.EMAIL} TEXT," +
                    "${AppTable.Transaction.NOMINAL} TEXT," +
                    "${AppTable.Transaction.BALANCE} TEXT," +
                    "${AppTable.Transaction.CATEGORY} TEXT," +
                    "${AppTable.Transaction.DESCRIPTION} TEXT," +
                    "${AppTable.Transaction.PICTURE} TEXT," +
                    "${AppTable.Transaction.TYPE} TEXT," +
                    "${AppTable.Transaction.CREATED_AT} TEXT)"

        private const val SQL_DELETE_TRANSACTION = "DROP TABLE IF EXISTS ${AppTable.Transaction.TABLE_NAME}"


        private const val SQL_CREATE_CATEGORY =
            "CREATE TABLE ${AppTable.Category.TABLE_NAME} (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${AppTable.Category.EMAIL} TEXT," +
                    "${AppTable.Category.CATEGORY} TEXT," +
                    "${AppTable.Category.CREATED_AT} TEXT)"

        private const val SQL_DELETE_CATEGORY = "DROP TABLE IF EXISTS ${AppTable.Category.TABLE_NAME}"

    }
}

package com.da.accons.databases

import android.provider.BaseColumns

/**
 * Created by Dimas Alamsyah on 11/4/2019.
 */
object AppTable {
    // Table contents are grouped together in an anonymous object.
    object Transaction : BaseColumns {
        const val TABLE_NAME = "transaction"
        const val EMAIL = "email"
        const val NOMINAL = "nominal"
        const val BALANCE = "balance"
        const val CATEGORY = "category"
        const val DESCRIPTION = "description"
        const val PICTURE = "picture"
        const val TYPE = "type"
        const val CREATED_AT = "created_at"
    }

    object Category : BaseColumns {
        const val TABLE_NAME = "category"
        const val EMAIL = "email"
        const val CATEGORY = "category"
        const val CREATED_AT = "created_at"
    }

}
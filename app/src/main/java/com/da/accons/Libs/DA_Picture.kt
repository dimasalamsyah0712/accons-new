package com.da.accons.Libs

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.FileProvider
import java.io.File
import java.io.IOException
import java.nio.file.Files.createFile
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Dimas Alamsyah on 10/19/2019.
 */
class DA_Picture(context: Context?){

    val REQUEST_IMAGE_CAPTURE = 11
    val REQUEST_CAMERA = 12

    private var mCurrentPhotoPath: String? = null;
    private var ctx: Context? = null
    private var activity: Activity

    init {
        ctx = context
        activity = ctx as Activity
    }

    @Throws(IOException::class)
    public fun createFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = ctx?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = absolutePath
        }
    }

    public fun getPicturePath(): String?{
        return mCurrentPhotoPath
    }

    fun takePicture(){
        val intent: Intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFile()

        val uri: Uri = FileProvider.getUriForFile(
            activity,
            "com.da.accons.fileprovider",
            file
        )

        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri)
        activity.startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

}
package com.da.accons.Libs

import android.text.Editable

/**
 * Created by Dimas Alamsyah on 10/27/2019.
 */
class StringUtil {
    companion object {
        fun String.toEdit(): Editable =  Editable.Factory.getInstance().newEditable(this)
    }

}
package com.da.accons.Libs.Dialog

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import com.da.accons.R
import com.da.accons.databinding.DialogSuccessBinding

/**
 * Created by Dimas Alamsyah on 10/17/2019.
 */
class DA_Dialog(ctx: Context?) {

    private var layoutInflater: LayoutInflater? = null
    private var view: View? = null

    private var btnOke: Button? = null
    private var msg: TextView? = null
    private var img: ImageView? = null
    private var linear: LinearLayout? = null
    private var progress: ProgressBar? = null

    private var btnCancel: Button? = null
    private var builder: AlertDialog.Builder? = null
    private var context: Context? = null
    private var alertDialog: AlertDialog? = null

    init {
        this.context = ctx
        builder = AlertDialog.Builder(ctx!!)
        alertDialog = builder?.create()
    }

    fun dismiss(){
        alertDialog?.dismiss()
    }

    fun show(){
        alertDialog = getDialogSuccess()
        alertDialog?.show()
        alertDialog?.window?.setLayout(800, 600)
        alertDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    fun getDialogSuccess(): AlertDialog? {

        return alertDialog
    }

    fun setContent(message: String, btntext: String, image: Int){

        linear?.visibility = View.VISIBLE
        progress?.visibility = View.GONE

        msg?.text = message
        img?.setImageResource(image)
        btnOke?.text = btntext
    }

    inner class DialogStandart(activity: Activity, onClickListener: View.OnClickListener){
        lateinit var activity: Activity
        lateinit var okListener: View.OnClickListener

        init {
            this.activity = activity
            this.okListener = onClickListener

            layoutInflater = activity?.layoutInflater
            view = layoutInflater?.inflate(R.layout.dialog_success, null)

            msg =  view?.findViewById(R.id.msg)
            img =  view?.findViewById(R.id.imageView)
            progress =  view?.findViewById(R.id.progressBar)
            linear =  view?.findViewById(R.id.linearLayout)
            btnOke =  view?.findViewById(R.id.btnOke)
        }

        fun showLoading(){

            linear?.visibility = View.GONE

            btnOke?.setOnClickListener(okListener)

            builder?.setView(view)
            alertDialog = builder?.create()
            alertDialog?.setCancelable(true)

            alertDialog?.show()
            alertDialog?.window?.setLayout(800, 600)
            alertDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    inner class CustomDialog(activity: FragmentActivity?, onClickListener: View.OnClickListener){

        private var activity: Activity? = null
        private var okListener: View.OnClickListener? = null

        init{
            this.activity = activity
            this.okListener = onClickListener

            layoutInflater = activity?.layoutInflater
            view = layoutInflater?.inflate(R.layout.dialog_success, null)

            msg =  view?.findViewById(R.id.msg)
            img =  view?.findViewById(R.id.imageView)
            progress =  view?.findViewById(R.id.progressBar)
            linear =  view?.findViewById(R.id.linearLayout)
            btnOke =  view?.findViewById(R.id.btnOke)
        }

        fun dismiss(){
            alertDialog?.dismiss()
        }

        fun getProgress(){

            linear?.visibility = View.GONE

            btnOke?.setOnClickListener(okListener)

            builder?.setView(view)
            alertDialog = builder?.create()
            alertDialog?.setCancelable(false)
        }

        fun showMsg(){
            linear?.visibility = View.VISIBLE
            progress?.visibility = View.GONE
        }

        fun startAgain(){
            progress?.visibility = View.VISIBLE
            linear?.visibility = View.GONE
        }

        fun setDialogWithBtn(message: String, btntext: String, image: Int){
            msg?.text = message
            img?.setImageResource(image)
            btnOke?.text = btntext
        }

        fun setDialog(message: String, image: Int){

            msg?.text = message
            img?.setImageResource(image)

            /*btnOke?.setOnClickListener(okListener)

            builder?.setView(view)
            alertDialog = builder?.create()
            alertDialog?.setCancelable(false)*/

            //val binding: DialogSuccessBinding = DataBindingUtil.inflate(this!!.layoutInflater, R.layout.dialog_success, container, false)

        }

    }

}